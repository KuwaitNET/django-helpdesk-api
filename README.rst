===============================
django-helpdesk-api
===============================

.. image:: https://badge.fury.io/py/django_helpdesk_api.png
    :target: http://badge.fury.io/py/django_helpdesk_api

.. image:: https://pypip.in/d/django_helpdesk_api/badge.png
    :target: https://crate.io/packages/django_helpdesk_api?version=latest


Api for the django helpdesk project

* Free software: BSD license

Requirements
------------

* Django 2.2+
* Python 3.6+

Installation
----------------------------

#. Clone the git repository.

#. Install all third party packages by running::

    $ pip install -r requirements.txt

#. Apply migrations::

    $ python manage.py migrate


Ticket API
----------