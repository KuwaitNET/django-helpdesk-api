from rest_framework import routers
from django.urls import path, include
from KB_api.views import KBItemViewSet, KBCategoryViewSet

app_name = "KB_api"

router = routers.DefaultRouter()
router.register(r"kbitems", KBItemViewSet)
router.register(r"kb-categories", KBCategoryViewSet)
urlpatterns = [
    path("", include(router.urls)),
]
