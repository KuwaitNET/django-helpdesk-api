from django.apps import AppConfig


class KbApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'KB_api'
