# Knowledge Base(KB) API

This API has multiple endpoints that carries out operations on Knowledge Base(KB) items.

# Endpoints

* Get a list of Knowledge base items

    - URL : `api/helpdesk/kbitems/`
    
    - Method : GET

* Add a Knowledge base item.

    - URL : `/api/helpdesk/kbitems/`

    - Method : POST

    - Required fields : title, question, answer, category.

    - Example of a request body

        ```json
        {           
            "title": "Knowledge base item 1",
            "question": "This is a question",
            "answer": "This is a answer",
            "category": 2
        }
        ```

* View a Knowledge Base item

    - URL : `/api/helpdesk/kbitems/<kbitem_id>`

    - Method : GET

* Remove a Knowledge Base item
    - URL : `/api/helpdesk/kbitems/<kbitem_id>/`

    - Method : DELETE

* View all Knowledge Base categories.

    - URL : `api/helpdesk/kb-categories/`

    - Method : GET

* Add a Knowledge Base category

    - URL : `api/helpdesk/kb-categories/`

    - Method : POST

    - Required fields : name, title, slug, description

* Delete a Knowledge Base category

    - URL : `api/helpdesk/kb-categories/`

    - Method : DELETE

* Vote for a Knowledge Base item.

    - URL : `/api/helpdesk/kbitems/<item_id>/vote/?vote=value`

    - Note : Vote value must be up or down, to upvote or downvote a KB item respectively. Voting for a Knowledge Base item will remove your downvote for the item.
    
    - Method : GET