from django.utils.translation import ugettext as _
from django.contrib.auth import get_user_model
from helpdesk.models import KBCategory, KBItem
from helpdesk import settings as helpdesk_settings
from rest_framework import viewsets, status, permissions
from KB_api.serializers import KBItemSerializer, KBCategorySerializer
from rest_framework.decorators import action
from rest_framework.response import Response

User = get_user_model()


class IsNotModifying(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        if not (user.is_superuser and user.is_active and user.is_staff):
            if view.action in ["create", "partial_update", "destroy", "update"]:
                return False
        return True


class KBItemViewSet(viewsets.ModelViewSet):
    basename = "KBItem"
    queryset = KBItem.objects.all().order_by("-last_updated")
    serializer_class = KBItemSerializer
    permission_classes = [IsNotModifying]

    @action(
        methods=["get"],
        detail=True,
        url_path="vote",
        url_name="vote",
    )
    def kbitem_vote(self, request, pk=None):
        item = KBItem.objects.filter(id=pk).first()
        vote = request.GET.get("vote", None)
        if not item:
            return Response(
                {"status": _("No KB item found with id {id}".format(id=pk))},
                status=status.HTTP_404_NOT_FOUND,
            )
        if vote == "up":
            if item.voted_by.filter(pk=request.user.pk):
                return Response(
                    {"status": _("You've already up voted this item")},
                    status=status.HTTP_200_OK,
                )
            if not item.voted_by.filter(pk=request.user.pk):
                item.votes += 1
                item.voted_by.add(request.user.pk)
                item.recommendations += 1
            if item.downvoted_by.filter(pk=request.user.pk):
                item.votes -= 1
                item.downvoted_by.remove(request.user.pk)
            item.save()
            return Response(
                {"status": _("Voted for KB item with id {id}".format(id=pk))},
                status=status.HTTP_200_OK,
            )
        if vote == "down":
            if item.downvoted_by.filter(pk=request.user.pk):
                return Response(
                    {"status": _("You've already down voted this item")},
                    status=status.HTTP_200_OK,
                )
            if not item.downvoted_by.filter(pk=request.user.pk):
                item.votes += 1
                item.downvoted_by.add(request.user.pk)
                item.recommendations -= 1
            if item.voted_by.filter(pk=request.user.pk):
                item.votes -= 1
                item.voted_by.remove(request.user.pk)
            item.save()
            return Response(
                {"status": _("Down voted KB item with id {id}".format(id=pk))},
                status=status.HTTP_200_OK,
            )
        return Response(
            {"status": _("vote value should be either up or down")},
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["get"],
        detail=False,
        url_path="category/(?P<slug>[A-Za-z0-9_-]+)",
        url_name="kbcategory-items",
    )
    def kbitem_by_kbcategory(self, request, slug, pk=None):
        category = slug
        kb_category = KBCategory.objects.filter(slug=category).first()
        if not kb_category:
            return Response(
                {"status": _("Category not found.")}, status=status.HTTP_404_NOT_FOUND
            )
        kbitems_by_category = KBItem.objects.filter(category=kb_category)
        kbitem_serializer = KBItemSerializer(kbitems_by_category, many=True)
        return Response(kbitem_serializer.data, status=status.HTTP_200_OK)


class KBCategoryViewSet(viewsets.ModelViewSet):
    basename = "KBCategory"
    queryset = KBCategory.objects.all()
    serializer_class = KBCategorySerializer
    permission_classes = [IsNotModifying]
