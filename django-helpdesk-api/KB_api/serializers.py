from rest_framework import serializers
from helpdesk.models import KBCategory, KBItem


class KBItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = KBItem
        fields = [
            "id",
            "title",
            "question",
            "answer",
            "category",
            "voted_by",
            "downvoted_by",
            "votes",
        ]
        read_only_fields = ["voted_by", "downvoted_by", "votes"]


class KBCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = KBCategory
        fields = ["name", "title", "slug", "description"]
