from rest_framework import routers
from django.urls import path, include
from tickets_api.views import TicketViewSet, QueueViewSet

app_name = "tickets_api"

router = routers.DefaultRouter()
router.register(r"tickets", TicketViewSet)
router.register(r"queues", QueueViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
