import json
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework import serializers
from django.core.exceptions import ValidationError
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _
from helpdesk.templated_email import send_templated_mail
from helpdesk.models import (
    Ticket,
    TicketCC,
    TicketChange,
    TicketDependency,
    FollowUpAttachment,
    FollowUp,
    KBItem,
    Queue,
)
from rest_framework import viewsets, permissions
from tickets_api.serializers import (
    TicketSerializer,
    TicketCCSerializer,
    TicketDependencySerializer,
    QueueSerializer,
)
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from helpdesk.lib import process_attachments, safe_template_context
from helpdesk import settings as helpdesk_settings
from helpdesk.user import huser_from_request


User = get_user_model()


class IsCreationOrIsAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        if not user.is_active:
            return False
        if user.is_superuser or user.is_staff:
            return True
        if helpdesk_settings.HELPDESK_ALLOW_NON_STAFF_TICKET_UPDATE:
            return True
        if view.action in ["create", "list", "retrieve", "statistics"]:
            return True
        return False


class TicketViewSet(viewsets.ModelViewSet):
    basename = "ticket"

    queryset = Ticket.objects.all().order_by("-created")
    serializer_class = TicketSerializer
    permission_classes = [IsCreationOrIsAuthenticated]

    def get_queryset(self):
        helpdesk_user = huser_from_request(self.request)
        user = helpdesk_user.user
        if user.is_superuser:
            queryset = Ticket.objects.all()
        else:
            queryset = Ticket.objects.filter(
                Q(assigned_to=user) | Q(submitter_email=user.email)
            )
        statuses = {"opened": 1, "reopened": 2, "resolved": 3, "closed": 4, "merged": 5}
        status = self.request.query_params.get("status")
        ordering = self.request.query_params.get("ordering")
        queue = self.request.query_params.get("queue")
        if status is not None and status in statuses:
            queryset = queryset.filter(status=statuses[status])
        if queue:
            queue_id = int(queue)
            queryset = queryset.filter(queue=queue_id)
        if ordering:
            queryset = queryset.order_by(ordering)
        return queryset

    def perform_create(self, serializer):
        super(TicketViewSet, self).perform_create(serializer)
        ticket = serializer.instance
        if ticket.assigned_to:
            title = _("Ticket Opened & Assigned to {}".format(ticket.get_assigned_to))
        else:
            title = _("Ticket Opened")
        followup = FollowUp(
            ticket=ticket,
            title=title,
            date=timezone.now(),
            public=True,
            comment=ticket.description,
        )
        if ticket.assigned_to:
            followup.user = ticket.assigned_to
        followup.save()
        try:
            files = process_attachments(followup, self.request.FILES.getlist("attachment"))
        except ValidationError:
            raise serializers.ValidationError({'attachment': _('Unsupported file extension')})
        context = safe_template_context(ticket)
        context["comment"] = followup.comment
        roles = {
            "submitter": ("newticket_submitter", context),
            "new_ticket_cc": ("newticket_cc", context),
            "ticket_cc": ("newticket_cc", context),
        }
        if (
            ticket.assigned_to
            and ticket.assigned_to.usersettings_helpdesk.email_on_ticket_assign
        ):
            roles["assigned_to"] = ("assigned_owner", context)
        ticket.send(
            roles,
            fail_silently=False,
            files=files,
        )

    def perform_update(self, serializer, ticket=None, data=None):
        if not ticket:
            ticket = self.get_object()
        if not data:
            data = self.request.data
        comment = data.get("comment", "")
        new_status = data.get("status", ticket.status)
        title = data.get("title", ticket.title)
        priority = data.get("priority", ticket.priority)
        due_date = data.get("due_date", ticket.due_date)
        owner = int(data.get("owner", -1))
        time_spent = data.get("time_spent", None)
        public = data.get("public", False)
        kbitem = data.get("kbitem", None)
        if kbitem and kbitem != ticket.kbitem:
            f = FollowUp(
                ticket=ticket,
                date=timezone.now(),
                title=_("KBItem set in update"),
                public=False,
                user=self.request.user,
            )
            f.save()
            ticket.kbitem = KBItem.objects.get(id=kbitem)
        no_changes = all(
            [
                not self.request.FILES,
                not comment,
                new_status == ticket.status,
                title == ticket.title,
                priority == int(ticket.priority),
                due_date == ticket.due_date,
                (owner == -1)
                or (not owner and not ticket.assigned_to)
                or (owner and User.objects.get(id=owner) == ticket.assigned_to),
            ]
        )
        if not no_changes:
            context = safe_template_context(ticket)
            from django.template import engines

            template_func = engines["django"].from_string
            comment = comment.replace("{%", "X-HELPDESK-COMMENT-VERBATIM").replace(
                "%}", "X-HELPDESK-COMMENT-ENDVERBATIM"
            )
            comment = comment.replace(
                "X-HELPDESK-COMMENT-VERBATIM", "{% verbatim %}{%"
            ).replace("X-HELPDESK-COMMENT-ENDVERBATIM", "%}{% endverbatim %}")
            comment = template_func(comment).render(context)
            if owner == -1 and ticket.assigned_to:
                owner = ticket.assigned_to.id
            f = FollowUp(
                ticket=ticket,
                date=timezone.now(),
                time_spent=time_spent,
                comment=comment,
            )
            f.user = self.request.user
            f.public = public
            reassigned = False
            old_owner = ticket.assigned_to
            if owner != -1:
                if owner != 0 and (
                    (ticket.assigned_to and owner != ticket.assigned_to.id)
                    or not ticket.assigned_to
                ):
                    new_user = User.objects.get(id=owner)
                    f.title = _("Assigned to %(username)s") % {
                        "username": new_user.get_username(),
                    }
                    ticket.assigned_to = new_user
                    serializer.instance.assigned_to = new_user
                    reassigned = True
                elif owner == 0 and ticket.assigned_to is not None:
                    f.title = _("Unassigned")
                    ticket.assigned_to = None
                    serializer.instance.assigned_to = None
            old_status_str = ticket.get_status_display()
            old_status = ticket.status
            if new_status != ticket.status:
                if (
                    new_status in (Ticket.RESOLVED_STATUS, Ticket.CLOSED_STATUS)
                    and not ticket.can_be_resolved
                ):
                    return Response(
                        status=status.HTTP_403_FORBIDDEN,
                    )
                ticket.status = new_status
                ticket.save()
                f.new_status = new_status
                if f.title:
                    f.title += _(" and %s") % ticket.get_status_display()
                else:
                    f.title = "%s" % ticket.get_status_display()
            if not f.title:
                if f.comment:
                    f.title = _("Comment")
                else:
                    f.title = _("Updated")
            f.save()
            files = []
            if self.request.FILES:
                files = process_attachments(f, self.request.FILES.getlist("attachment"))
            if title and title != ticket.title:
                c = TicketChange(
                    followup=f,
                    field=_("Title"),
                    old_value=ticket.title,
                    new_value=title,
                )
                c.save()
                ticket.title = title

            if new_status != old_status:
                c = TicketChange(
                    followup=f,
                    field=_("Status"),
                    old_value=old_status_str,
                    new_value=ticket.get_status_display(),
                )
                c.save()

            if ticket.assigned_to != old_owner:
                c = TicketChange(
                    followup=f,
                    field=_("Owner"),
                    old_value=old_owner,
                    new_value=ticket.assigned_to,
                )
                c.save()

            if priority != ticket.priority:
                c = TicketChange(
                    followup=f,
                    field=_("Priority"),
                    old_value=ticket.priority,
                    new_value=priority,
                )
                c.save()
                ticket.priority = priority

            if due_date != ticket.due_date:
                c = TicketChange(
                    followup=f,
                    field=_("Due on"),
                    old_value=ticket.due_date,
                    new_value=due_date,
                )
                c.save()
                ticket.due_date = due_date

            if new_status in (Ticket.RESOLVED_STATUS, Ticket.CLOSED_STATUS):
                if new_status == Ticket.RESOLVED_STATUS or ticket.resolution is None:
                    ticket.resolution = comment

            context = safe_template_context(ticket)
            context.update(
                resolution=ticket.resolution,
                comment=f.comment,
            )
            messages_sent_to = set()
            try:
                messages_sent_to.add(self.request.user.email)
            except AttributeError:
                pass
            if f.comment or (
                f.new_status in (Ticket.RESOLVED_STATUS, Ticket.CLOSED_STATUS)
            ):
                if f.new_status == Ticket.RESOLVED_STATUS:
                    template = "resolved_"
                elif f.new_status == Ticket.CLOSED_STATUS:
                    template = "closed_"
                else:
                    template = "updated_"

                roles = {
                    "submitter": (template + "submitter", context),
                    "ticket_cc": (template + "cc", context),
                }
                if (
                    ticket.assigned_to
                    and ticket.assigned_to.usersettings_helpdesk.email_on_ticket_change
                ):
                    roles["assigned_to"] = (template + "cc", context)
                messages_sent_to.update(
                    ticket.send(
                        roles,
                        dont_send_to=messages_sent_to,
                        fail_silently=False,
                        files=files,
                    )
                )

            if reassigned:
                template_staff = "assigned_owner"
            elif f.new_status == Ticket.RESOLVED_STATUS:
                template_staff = "resolved_owner"
            elif f.new_status == Ticket.CLOSED_STATUS:
                template_staff = "closed_owner"
            else:
                template_staff = "updated_owner"

            if ticket.assigned_to and (
                ticket.assigned_to.usersettings_helpdesk.email_on_ticket_change
                or (
                    reassigned
                    and ticket.assigned_to.usersettings_helpdesk.email_on_ticket_assigned
                )
            ):
                messages_sent_to.update(
                    ticket.send(
                        {"assigned_to": (template_staff, context)},
                        dont_send_to=messages_sent_to,
                        fail_silently=False,
                        files=files,
                    )
                )

            if reassigned:
                template_cc = "assigned_cc"
            elif f.new_status == Ticket.RESOLVED_STATUS:
                template_cc = "resolved_cc"
            elif f.new_status == Ticket.CLOSED_STATUS:
                template_cc = "closed_cc"
            else:
                template_cc = "updated_cc"

            messages_sent_to.update(
                ticket.send(
                    {"ticket_cc": (template_cc, context)},
                    dont_send_to=messages_sent_to,
                    fail_silently=False,
                    files=files,
                )
            )
        ticket.save()
        serializer.instance = ticket
        serializer.save()

    @action(
        methods=["get"],
        detail=True,
        url_path="cc",
        url_name="cc",
    )
    def get_cc_by_id(self, request, pk=None):
        ticket_obj = Ticket.objects.get(id=pk)
        ticketcc_objs = TicketCC.objects.filter(ticket=ticket_obj)
        if not ticketcc_objs:
            return Response(
                {"status": _("No ticket cc found.")}, status=status.HTTP_404_NOT_FOUND
            )
        serializer = TicketCCSerializer(ticketcc_objs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        methods=["post"],
        detail=True,
        url_path="cc/add",
        url_name="add-cc",
    )
    def add_cc(self, request, pk=None):
        """Add a TicketCC object to an existing Ticket."""
        ticket_obj = Ticket.objects.get(id=pk)
        if not ticket_obj:
            return Response(
                {"status": _("No ticket with id {id} found.".format(id=pk))},
                status=status.HTTP_404_NOT_FOUND,
            )

        ticketcc_data = request.data
        serializer = TicketCCSerializer(data=ticketcc_data)
        if serializer.is_valid():
            email = serializer.validated_data.get("email")
            user = serializer.validated_data.get("user")
            can_view = serializer.validated_data.get("can_view")
            can_update = serializer.validated_data.get("can_update")

            if (
                email
                and TicketCC.objects.filter(ticket=ticket_obj, email=email).exists()
            ):
                return Response(
                    {"status": _("Cannot create a ticket CC with the same email ID.")},
                    status=status.HTTP_403_FORBIDDEN,
                )

            if user and TicketCC.objects.filter(ticket=ticket_obj, user=user).exists():
                return Response(
                    {"status": _("Cannot create a ticket CC with the same user ID.")},
                    status=status.HTTP_403_FORBIDDEN,
                )
            if not email and not user:
                return Response(
                    {"status": _("Specify either an email ID or a user ID")},
                    status=status.HTTP_403_FORBIDDEN,
                )
            ticketcc_obj = TicketCC(
                ticket=ticket_obj,
                email=email,
                user=user,
            )
            if can_view:
                ticketcc_obj.can_view = can_view
            if can_update:
                ticketcc_obj.can_update = can_update
            ticketcc_obj.save()
            return Response(
                {
                    "status": _("Ticket CC successfully created"),
                    "data": model_to_dict(ticketcc_obj),
                },
                status=status.HTTP_200_OK,
            )
        else:
            return Response(
                {"status": _("Invalid request.")}, status=status.HTTP_404_NOT_FOUND
            )

    @action(
        methods=["delete"],
        detail=True,
        url_path="cc/delete",
        url_name="delete-cc",
    )
    def delete_cc(self, request, pk=None):
        ticketcc_id = self.request.query_params.get("cc_id")
        ticket_obj = Ticket.objects.filter(id=pk).first()
        if not ticket_obj:
            return Response(
                {"status": _("No ticket with id {id} found.".format(id=pk))},
                status=status.HTTP_404_NOT_FOUND,
            )
        ticketcc_obj = TicketCC.objects.filter(
            id=ticketcc_id, ticket=ticket_obj
        ).first()
        if not ticketcc_obj:
            return Response(
                {
                    "status": _(
                        "No ticket CC with id {id} found.".format(id=ticketcc_id)
                    )
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        ticketcc_obj.delete()
        return Response({"status": _("Deleted ticket CC")}, status=status.HTTP_200_OK)

    @action(
        methods=["post"],
        detail=True,
        url_path="dependency/add",
        url_name="add-dependency",
    )
    def add_dependency(self, request, pk=None):
        depends_on_id = self.request.query_params.get("dependency_id")
        if not depends_on_id:
            return Response(
                {"status": _("Missing query param for dependency id")},
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )
        ticket_id = pk
        if depends_on_id == ticket_id:
            return Response(
                {
                    "status": _(
                        "Cannot create dependencies between two identical tickets."
                    )
                },
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )
        ticket = Ticket.objects.filter(id=ticket_id).first()
        depends_on = Ticket.objects.filter(id=depends_on_id).first()
        if not ticket or not depends_on:
            return Response(
                {"status": _("Cannot find ticket")},
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )
        ticket_dep = TicketDependency(ticket=ticket, depends_on=depends_on)
        ticket_dep.save()
        serializer = TicketDependencySerializer(instance=ticket_dep)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        methods=["delete"],
        detail=True,
        url_path="dependency/delete/(?P<dependency_id>\d+)",
        url_name="delete-dependency",
    )
    def delete_dependency(self, request, dependency_id, pk=None):
        ticket_id = pk
        depends_on_id = dependency_id
        ticket = Ticket.objects.filter(id=ticket_id).first()
        if not ticket:
            return Response(
                {"status": _("Cannot find ticket")},
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )
        ticket_dep = TicketDependency.objects.filter(ticket=ticket, id=depends_on_id)
        if not ticket_dep:
            return Response(
                {"status": _("Cannot find ticket dependency")},
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )
        ticket_dep.delete()
        return Response(
            {"status": _("Dependency deleted successfully")},
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["get"],
        detail=True,
        url_path="hold",
        url_name="holding-ticket",
    )
    def hold_ticket(self, request, pk=None):
        ticket = get_object_or_404(Ticket, id=pk)
        if ticket.on_hold:
            return Response(
                {"status": _("Ticket is already on hold")},
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )
        ticket.on_hold = True
        title = _("Ticket placed on hold")
        f = FollowUp(
            ticket=ticket,
            user=request.user,
            title=title,
            date=timezone.now(),
            public=True,
        )
        f.save()
        ticket.save()
        return Response(
            {"ticket_url": ticket.get_absolute_url()}, status=status.HTTP_200_OK
        )

    @action(
        methods=["get"],
        detail=True,
        url_path="unhold",
        url_name="unholding-ticket",
    )
    def unhold_ticket(self, request, pk=None):
        ticket = get_object_or_404(Ticket, id=pk)
        if not ticket.on_hold:
            return Response(
                {"status": _("Ticket is already off hold")},
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )
        ticket.on_hold = False
        title = _("Ticket taken off hold")
        f = FollowUp(
            ticket=ticket,
            user=request.user,
            title=title,
            date=timezone.now(),
            public=True,
        )
        f.save()
        ticket.save()
        return Response(
            {"ticket_url": ticket.get_absolute_url()}, status=status.HTTP_200_OK
        )

    @action(
        methods=["POST"],
        detail=False,
        url_path="bulk-update",
        url_name="bulk-update",
    )
    def bulk_update(self, request):
        tickets_data = json.loads(request.body.decode())
        response_data = []
        for ticket_data in tickets_data:
            instance = Ticket.objects.get(id=ticket_data["id"])
            serializer = self.get_serializer(
                instance, data=ticket_data["attributes"], partial=True
            )
            serializer.is_valid(raise_exception=True)
            self.perform_update(
                serializer, ticket=instance, data=ticket_data["attributes"]
            )
            response_data.append(serializer.data)

        return Response(response_data, status=status.HTTP_200_OK)

    @action(
        methods=["POST"],
        detail=False,
        url_path="merge",
        url_name="merge",
    )
    def merge_tickets(self, request):
        data = json.loads(request.body.decode())
        main_id = data.pop(0)["id"]
        chosen_ticket = Ticket.objects.get(id=main_id)
        # data_for_updating = {}
        for ticket_data in data:
            ticket = Ticket.objects.get(id=ticket_data["id"])
            for attr in ticket_data["attributes"]:
                value = getattr(ticket, attr)
                setattr(chosen_ticket, attr, value)
            chosen_ticket.save()
            ticket.merged_to = chosen_ticket
            ticket.status = Ticket.DUPLICATE_STATUS
            ticket.save()
            ticket.followup_set.update(
                ticket=chosen_ticket,
                title=_("[Merged from #%(id)d] %(title)s")
                % {"id": ticket.id, "title": ticket.title},
            )
            context = safe_template_context(ticket)
            if ticket.submitter_email:
                send_templated_mail(
                    template_name="merged",
                    context=context,
                    recipients=[ticket.submitter_email],
                    bcc=[
                        cc.email_address
                        for cc in ticket.ticketcc_set.select_related("user")
                    ],
                    sender=ticket.queue.from_address,
                    fail_silently=True,
                )
            chosen_ticket.add_email_to_ticketcc_if_not_in(email=ticket.submitter_email)
            if ticket.assigned_to and ticket.assigned_to.email:
                chosen_ticket.add_email_to_ticketcc_if_not_in(
                    email=ticket.assigned_to.email
                )
            for ticketcc in ticket.ticketcc_set.all():
                chosen_ticket.add_email_to_ticketcc_if_not_in(ticketcc=ticketcc)
        return Response(model_to_dict(chosen_ticket), status=status.HTTP_200_OK)

    @action(
        methods=["delete"],
        detail=True,
        url_path="attachment-delete/(?P<attachment_id>\d+)",
        url_name="delete-attachment",
    )
    def delete_attachment(self, request, attachment_id, pk=None):
        ticket = get_object_or_404(Ticket, id=pk)
        attachment = get_object_or_404(FollowUpAttachment, id=attachment_id)
        attachment.delete()
        return Response(
            {"ticket_url": ticket.get_absolute_url()}, status=status.HTTP_200_OK
        )

    @action(
        methods=["post"],
        detail=True,
        url_path="followup-edit/(?P<followup_id>\d+)",
        url_name="edit-followup",
    )
    def edit_followup(self, request, followup_id, pk=None):
        ticket = get_object_or_404(Ticket, id=pk)
        followup = get_object_or_404(FollowUp, id=followup_id)
        if not followup in ticket.followup_set.all():
            return Response(
                {"error": _("The specified followup for this ticket is not found")},
                status=status.HTTP_404_NOT_FOUND,
            )
        if followup.user and request.user != followup.user:
            return Response(
                {
                    "error": _(
                        "You cannot edit this followup cause you're not its creator"
                    )
                },
                status=status.HTTP_401_UNAUTHORIZED,
            )
        if followup.ticketchange_set.all():
            return Response(
                {"error": _("This follow up has ticket changes and cannot be edited")},
                status=status.HTTP_403_FORBIDDEN,
            )
        data = json.loads(request.body.decode())
        allowed_attrs = [
            "title",
            "public",
            "new_status",
            "ticket",
            "new_status",
            "time_spent",
        ]
        if not all(elem in allowed_attrs for elem in list(data.keys())):
            return Response(
                {"error": _("Use allowed attributes only")},
                status=status.HTTP_403_FORBIDDEN,
            )
        FollowUp.objects.filter(id=followup_id).update(**data)
        return Response(
            model_to_dict(FollowUp.objects.get(id=followup_id)),
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["delete"],
        detail=True,
        url_path="followup-delete/(?P<followup_id>\d+)",
        url_name="delete-followup",
    )
    def delete_followup(self, request, followup_id, pk=None):
        ticket = get_object_or_404(Ticket, id=pk)
        followup = get_object_or_404(FollowUp, id=followup_id)
        if not followup in ticket.followup_set.all():
            return Response(
                {"error": _("The specified followup for this ticket is not found")},
                status=status.HTTP_404_NOT_FOUND,
            )
        if not request.user.is_superuser:
            return Response(
                {"error": _("You cannot delete this follow up")},
                status=status.HTTP_401_UNAUTHORIZED,
            )
        if not helpdesk_settings.HELPDESK_SHOW_DELETE_BUTTON_SUPERUSER_FOLLOW_UP:
            return Response(
                {
                    "error": _(
                        "HELPDESK_SHOW_DELETE_BUTTON_SUPERUSER_FOLLOW_UP is false"
                    )
                },
                status=status.HTTP_403_FORBIDDEN,
            )
        FollowUp.objects.get(id=followup_id).delete()
        return Response(
            {"message": _("Deleted Successfully")},
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["get"],
        detail=False,
        url_path="statistics",
        url_name="statistics",
    )
    def statistics(self, request):
        user = request.user
        if user.is_superuser:
            tickets = Ticket.objects.all()
            overall_tickets = tickets.count()
            opened_tickets = tickets.filter(status=1).count()
            reopened_tickets = tickets.filter(status=2).count()
            resolved_tickets = tickets.filter(status=3).count()
            closed_tickets = tickets.filter(status=4).count()
            merged_tickets = tickets.filter(status=5).count()
        else:
            tickets = Ticket.objects.filter(
                Q(assigned_to=user) | Q(submitter_email=user.email)
            )
            overall_tickets = tickets.count()
            opened_tickets = tickets.filter(status=1).count()
            reopened_tickets = tickets.filter(status=2).count()
            resolved_tickets = tickets.filter(status=3).count()
            closed_tickets = tickets.filter(status=4).count()
            merged_tickets = tickets.filter(status=5).count()
        data = {
            "opened": opened_tickets,
            "reopened": reopened_tickets,
            "resolved": resolved_tickets,
            "closed": closed_tickets,
            "merged": merged_tickets,
            "overall": overall_tickets,
        }
        return Response(data, status=status.HTTP_200_OK)


class QueueViewSet(viewsets.ModelViewSet):
    basename = "queue"

    queryset = Queue.objects.all()
    serializer_class = QueueSerializer
    permission_classes = [IsCreationOrIsAuthenticated]
