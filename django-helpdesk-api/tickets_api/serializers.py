from django.conf import settings
from rest_framework import serializers
from helpdesk.models import (
    Ticket,
    TicketCC,
    TicketDependency,
    Queue,
    KBItem,
    FollowUp,
    FollowUpAttachment,
)
from django.contrib.auth import get_user_model


User = get_user_model()


class TicketSerializer(serializers.ModelSerializer):
    """
    Serializer for Ticket Model.
    """

    assigned_to_user = serializers.SerializerMethodField()
    kbitem = serializers.SerializerMethodField()
    kbitem_id = serializers.SerializerMethodField()
    attachment = serializers.SerializerMethodField()

    def get_assigned_to_user(self, obj):
        if obj.assigned_to:
            assigned_to_id = obj.assigned_to.id
            return User.objects.filter(id=assigned_to_id).first().username
        else:
            return None

    def get_kbitem(self, obj):
        if obj.kbitem:
            kbitem_id = obj.kbitem.id
            return KBItem.objects.filter(id=kbitem_id).first().title
        else:
            return None

    def get_kbitem_id(self, obj):
        if obj.kbitem:
            return obj.kbitem.id
        else:
            return None

    def get_attachment(self, obj):
        attachments_list = []
        followups = FollowUp.objects.filter(ticket=obj)
        for followup in followups:
            attachments_files = FollowUpAttachment.objects.filter(followup=followup)
            for attachment in attachments_files:
                attachments_list.append(
                    attachment.file.url
                )
        return attachments_list

    class Meta:
        model = Ticket
        fields = "__all__"
        extra_kwargs = {
            "description": {"required": True},
            "due_date": {"required": False},
            "submitter_email": {"required": True},
            "assigned_to": {"required": False},
        }


class TicketCCSerializer(serializers.ModelSerializer):
    """
    Serializer for TicketCC Model. ticket is optional here as creating a TicketCC EP is accessed using the id of the ticket.
    """

    class Meta:
        model = TicketCC
        fields = "__all__"
        extra_kwargs = {
            "user": {"required": False},
            "can_view": {"required": False},
            "can_update": {"required": False},
            "ticket": {"required": False},
        }


class TicketDependencySerializer(serializers.ModelSerializer):
    depends_on = TicketSerializer(many=False, required=True)

    class Meta:
        model = TicketDependency
        fields = ["depends_on"]
        extra_kwargs = {
            "ticket": {"required": False},
        }


class QueueSerializer(serializers.ModelSerializer):
    """
    Serializer for Queue Model.
    """

    class Meta:
        model = Queue
        fields = "__all__"


class FollowUpAttachmentSerializer(serializers.ModelSerializer):
    """
    Serializer for FollowUpAttachment Model.
    """

    class Meta:
        model = FollowUpAttachment
        fields = "__all__"