# Ticket API

This API has multiple endpoints that carries out operations on Tickets.

# Endpoints

* Obtain Authentication Token
    - URL : `/obtain-auth-token/`
    
    - Method : POST

    - Example of a request body

        ```json
        {
            "username": "ex_ticket",
            "password": "bigpassword"
        }
        ```

    - Example of a response        
    
        ```json
        {
            "token": "443356f603955caaf0a9085f34123b8f457dc4b62"
        }
        ```

    

* Get a list of tickets
    - URL : `/api/helpdesk/tickets/`

    - Method : GET

    - Example of a  response

        ```json
        {
            "count": 1,
            "next": null,
            "previous": null,
            "results": [
                {
                    "id": 5,
                    "title": "Sample ticket.",
                    "created": "2022-02-01T16:00:16.809362+03:00",
                    "modified": "2022-02-01T16:00:16.809362+03:00",
                    "submitter_email": null,
                    ...
                }
            ]
        }
        ```

* Add a ticket

    - URL : `/api/helpdesk/tickets/`
    
    - Method : POST

    - Example of a request body

        ```json
        {    
            "title": "Big problem",
            "description": "Its a problem",
            "queue": 1
        }
        ```

    - Example of a response

        ```json
        {
            "id": 7,
            "title": "Big problem",
            "created": "2022-02-09T14:32:18.423318+03:00",
            "modified": "2022-02-09T14:32:18.423318+03:00",
            "submitter_email": null,
            "status": 1,
            "on_hold": false,
            "description": "Its a problem",
            ...
        }
        ```

* View details of a ticket

    - URL : `/api/helpdesk/tickets/<id>/`

    - Method : GET

    - Example of a response

        ```json
        {
            "id": 7,
            "title": "Big problem",
            "created": "2022-02-09T14:32:18.423318+03:00",
            "modified": "2022-02-09T14:32:18.423318+03:00",
            "submitter_email": null,
            "status": 1,
            "on_hold": false,
            "description": "Its a problem",
            ...
        }
        ```

* Partially update a ticket

    - Use this endpoint to adjust minor details like description or title of a ticket. Does not require all fields.

    - URL : `/api/helpdesk/tickets/<id>/`

    - Method : PATCH

    - Example of a request body

        ```json
        {    
            "title": "Big trouble"
        }
        ```

    - Example of a response

        ```json
        {
            "id": 7,
            "title": "Big trouble",
            "created": "2022-02-09T14:32:18.423318+03:00",
            "modified": "2022-02-09T14:40:32.806964+03:00",
            "submitter_email": null,
            "status": 1,
            "on_hold": false,
            "description": "Its a problem",
            ...
        }
        ```

* Update a ticket
    - Use this endpoint to alter the title, queue and description of a ticket, along with any other fields to be updated.

    - URL : `/api/helpdesk/tickets/<id>/`

    - Method : PUT 

    - Example of a request body

        ```json
        {    
            "title": "Big trouble",
            "description": "Its a big trouble.",
            "queue": 1
        }
        ```
    
    - Example of a response

        ```json
        {
            "id": 7,
            "title": "Big trouble",
            "created": "2022-02-09T14:32:18.423318+03:00",
            "modified": "2022-02-09T14:47:18.712521+03:00",
            "submitter_email": null,
            "status": 1,
            "on_hold": false,
            "description": "Its a big trouble.",
            ...
        }
        ```

* Delete a ticket
    - URL : `/api/helpdesk/tickets/<id>/`

    - Method : DELETE

* Updating multiple tickets
    - URL : `/api/helpdesk/tickets/bulk-update/`

    - Method : POST

* Merging upto 3 tickets into one ticket.
    - URL : `/api/helpdesk/tickets/merge/`

    - Method : POST

* Put a ticket on hold.
    - URL : `/api/helpdesk/tickets/<id>/hold/`

    - Method : GET

* Remove hold on a ticket.
    - URL : `/api/helpdesk/tickets/<id>/unhold/`

    - Method : GET

* View all CCs for a ticket.
    - URL : `/api/helpdesk/tickets/<id>/cc/`

    - Method : GET

    - Example of a response
        ```json
        {
            "id": 2,
            "email": "email@email.com",
            "can_view": false,
            "can_update": false,
            "ticket": 8,
            "user": null
        }
        ```

* Add a CC for a ticket.
    - URL : `/api/helpdesk/tickets/<id>/cc/add/`

    - Method : POST

    - Example of a request body
        ```json
        {
            "email": "email@email.com"
        }
        ```
    
    - Example of a response
        ```json
        {
            "status": "Ticket CC successfully created"
        }
        ```

* Delete a CC for a ticket.
    - URL : `/api/helpdesk/tickets/<id>/cc/delete?cc_id=<cc_id>`

    - Method: DELETE

    - Example of a response
        ```json
        {
            "status": "Deleted ticket CC"
        }
        ```

* Add a dependency for a ticket.
    - Use this endpoint to create a dependency. The ticket with <dependency_id> is now dependent on ticket with <id> after executing the call.

    - URL : `/api/helpdesk/tickets/<id>/dependency/add/?dependency_id=<dependency_id>`

    - Method : PUT

    - Example of a response
        ```json
        {
            "depends_on": {
                "id": 5,
                "title": "Sample ticket",
                "created": "2022-02-01T16:00:16.809362+03:00",
                "modified": "2022-02-01T16:00:16.809362+03:00",
                "submitter_email": null,
                "status": 3,
                ...
            }
        }
        ```

* Delete a dependency for a ticket.
    - URL: `/api/helpdesk/tickets/<id>/dependency/delete/?ticket_id=<ticket_id>`

    - Method : DELETE

    - Example of a response
        ```json
        {
            "status": "Dependency deleted successfully"
        }
        ```

* Delete an attachment for a ticket.
    - URL : `/api/helpdesk/tickets/<ticket_id>/attachment_delete/<attachment_id>/`

    - Method : DELETE

* Edit a Followup for a ticket.
    - URL: `/api/helpdesk/tickets/<id>/followup-edit/<followup_id>`

    - Method : POST

    - Example of a response
        ```json
        {
            "id": 274,
            "ticket": 85,
            "date": "2022-02-27T20:50:50.597314Z",
            "title": "Comment",
            "comment": "come comment",
            "public": true,
            "user": 1,
            "new_status": null,
            "time_spent": null
        }
        ```

* Delete a Followup for a ticket.
    - URL: `/api/helpdesk/tickets/<id>/followup-delete/<followup_id>`

    - Method : DELETE
